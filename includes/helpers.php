<?php

	/**
	 *
	 * Select last order ID
	 *
	 */
	if(!function_exists('get_last_order_id')){

			function get_last_order_id(){
				$query = new WC_Order_Query( array(
					'limit' => 1,
					'orderby' => 'date',
					'order' => 'DESC',
					'return' => 'ids',
				) );
				
				$orders = $query->get_orders();

				if ( !$orders ) {
					return;
				}

				return ($orders[0]);
			}
	}

	//added code for product image for generate invoice.

	if(!function_exists('GetImageUrlsByProductId')){

		function GetImageUrlsByProductId( $productId){
	 
			$product = new WC_product($productId);
			$attachmentIds = $product->get_gallery_attachment_ids();
			$imgUrls = array();
			foreach( $attachmentIds as $attachmentId )
			{
				$imgUrls[] = wp_get_attachment_url( $attachmentId );
			}
		 
			return $imgUrls;
		}
	}