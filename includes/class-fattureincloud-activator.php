<?php

/**
 * Fired during plugin activation
 *
 * @link       https://mediusware.com
 * @since      1.0.0
 *
 * @package    Fattureincloud
 * @subpackage Fattureincloud/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Fattureincloud
 * @subpackage Fattureincloud/includes
 * @author     Mediusware <coredeveloper.2013@gmail.com>
 */
class Fattureincloud_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
