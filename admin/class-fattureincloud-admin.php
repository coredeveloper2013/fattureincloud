<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://mediusware.com
 * @since      1.0.0
 *
 * @package    Fattureincloud
 * @subpackage Fattureincloud/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Fattureincloud
 * @subpackage Fattureincloud/admin
 * @author     Mediusware <coredeveloper.2013@gmail.com>
 */
class Fattureincloud_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}



	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Fattureincloud_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Fattureincloud_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/fattureincloud-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Fattureincloud_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Fattureincloud_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/fattureincloud-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Creates the menu tabs
	 *
	 * @since 		1.0.0
	 * @return 		void
	 */

	public static function page_tabs( $current = 'ordine' ) {
		$tabs = array(
			'order'   => __( 'Order', 'fattureincloud' ),
			'settings'  => __( 'Settings', 'fattureincloud' ),
			'invoice' => __('Invoice', 'fattureincloud'),
			'email' => __('Email', 'fattureincloud')
		);
		$html = '<h2 class="nav-tab-wrapper">';
		foreach( $tabs as $tab => $name ){
			$class = ( $tab == $current ) ? 'nav-tab-active' : '';
			$url = admin_url( 'admin.php?page=fattureincloud&tab='.$tab);
			$html .= '<a class="nav-tab ' . $class . '" href="'.$url.'">' . $name . '</a>';
		}
		$html .= '</h2>';
		
		echo $html;
	}

	/**
	 * Creates the Add menu
	 *
	 * @since 		1.0.0
	 * @return 		void
	 */

	public function add_menu() {

		$parent_slug = 'woocommerce';
		$page_title  = 'WooCommerce Admin Page';
		$menu_title  = 'Fattureincloud';
		$capability  = 'manage_woocommerce';
		$menu_slug   = '';

		// Added Sub Menu Page For Fattureincloud 
		
		$sub_menu = add_submenu_page(
			$parent_slug,
			apply_filters( $this->plugin_name . $page_title, esc_html__( 'Fattureincloud WooCommerce Admin Page', 'fattureincloud' ) ),
			apply_filters( $this->plugin_name . $menu_title, esc_html__( 'Fattureincloud', 'fattureincloud' ) ),
			$capability,
			$this->plugin_name . $menu_slug,
			array( $this, 'page_options' )
		);
	
	} 

	/**
	 * Creates the options page
	 *
	 * @since 		1.0.0
	 * @return 		void
	 */
	public function page_options() {

		include( plugin_dir_path( __FILE__ ) . 'views/fattureincloud-admin-display.php' );

	} // page_options()
	
	/**
	 * Custom field add in the billing address 
	 *
	 * @since 		1.0.0
	 * @return 		void
	 */

	public function custom_admin_billing_fields($fields){

	  	$fields['cod_fisc'] = array(
			'label' => __('Fiscal Code','woocommerce'),
			'wrapper_class' => 'form-field-wide',
			'show' => true,

		);
		$fields['partita_iva'] = array(
			'label' => __('VAT number','woocommerce'),
			'wrapper_class' => 'form-field-wide',
			'show' => true,

		);

		return $fields;

	}	
	/**
	 * Custom field add in the billing address in the checkout page 
	 *
	 * @since 		1.0.0
	 * @return 		void
	 */

	public function custom_billing_fields($fields){

	  	$fields['billing_cod_fisc'] = array(
			'label'       => __('Fiscal Code','woocommerce'),
			'placeholder' => __('Write the Tax Code','woocommerce'),
			'required'    => true,
			'class'       => array('piva-number-class form-row-wide' ),
			'clear'		  => true

		);

		$fields['billing_partita_iva'] = array(
			'label'       => __('VAT number','woocommerce'),
			'placeholder' => __('Write the VAT number','woocommerce'),
			'required'    => false,
			'clear'       => true,
			'class'       => array( 'form-row' ),

		);

		return $fields;

	}

	/**
	 * If order is completed, The function generate automatic invoice to the fattureincloud.it
	 *
	 * @since 		1.0.0
	 * @return 		void
	 */

  public function fattureincloud_order_complete( $order_id ) {

  	update_option('mw_fattureincloud_order_id', $order_id );
  	
  	$order = wc_get_order( $order_id); // load order by id
  	$order_data = $order->get_data(); // get order data

  	$fattureincloud_result = $this->process_order_invoice_to_fatture($order, $order_data,'completed'); // send order to generate invoice

	if (!in_array("success", $fattureincloud_result)) {
		update_option('fattureincloud_autosent_id_fallito', $order_id);
	}

  }


  	public function mw_send_invoice_to_fattureincloud($order_id){

  		update_option('mw_fattureincloud_order_id', $order_id );

	  	$order = wc_get_order( $order_id); // load order by id
	  	$order_data = $order->get_data(); // get order data

	  	$fattureincloud_result = $this->process_order_invoice_to_fatture($order, $order_data,'completed'); // send order to generate invoice
		if ( !in_array("success", $fattureincloud_result)) {
			update_option('fattureincloud_autosent_id_fallito', $order_id);
		}

  	}
	/**
	 * the function parameter mandatory $order and  $order_data
	 *  
	 *
	 * @since 		1.0.0
	 * @return 		void
	 */

	public static function process_order_invoice_to_fatture($order, $order_data, $order_complete = 'completed'){

		$fattureincloud_url = "https://api.fattureincloud.it:443/v1/fatture/nuovo";

		$api_uid = get_option('api_uid_mfattureincloud');
		$api_key = get_option('api_key_mfattureincloud');

		$lista_articoli = array();

		$spedizione_lorda = $order_data['shipping_total'] + $order_data['shipping_tax'] ;
		$spedizione_netta = $spedizione_lorda - $order_data['shipping_tax'];

		$codice_iva = 0;
		
		$id_ordine_scelto = $order_data['id'];
		
		if (get_post_meta( $id_ordine_scelto, '_billing_piva', true ) || get_post_meta( $id_ordine_scelto, '_billing_cf', true ) ) {

			$order_billing_partiva = get_post_meta( $id_ordine_scelto, '_billing_piva', true ) ;
			$order_billing_codfis = get_post_meta( $id_ordine_scelto, '_billing_cf', true ) ;
		}
		elseif ( get_post_meta( $id_ordine_scelto, '_billing_partita_iva', true ) || $order_billing_codfis = get_post_meta( $id_ordine_scelto, '_billing_cod_fisc', true )) {
			$order_billing_partiva = get_post_meta( $id_ordine_scelto, '_billing_partita_iva', true ) ;
			$order_billing_codfis = get_post_meta( $id_ordine_scelto, '_billing_cod_fisc', true ) ;
		}
		else {
			$order_billing_partiva ="";
			$order_billing_codfis = "";
		}		
		

		$order_billing_first_name = $order_data['billing']['first_name'];
		$order_billing_last_name = $order_data['billing']['last_name'];
		$order_billing_company = $order_data['billing']['company'];
		$order_billing_address_1 = $order_data['billing']['address_1'];
		$order_billing_address_2 = $order_data['billing']['address_2'];
		$order_billing_city = $order_data['billing']['city'];
		$order_billing_state = $order_data['billing']['state'];
		$order_billing_postcode = $order_data['billing']['postcode'];
		$order_billing_country = $order_data['billing']['country'];
		$order_billing_email = $order_data['billing']['email'];
		$order_billing_phone = $order_data['billing']['phone'];
		$order_billing_method = $order_data['payment_method_title'];

		foreach ($order->get_items() as $item_key => $item_values):

		    $item_data = $item_values->get_data();


		    $line_total = $item_data['total'];


		    $product_id = $item_values->get_product_id(); // the Product id
		    $wc_product = $item_values->get_product(); // the WC_Product object
		    ## Access Order Items data properties (in an array of values) ##
		    $item_data = $item_values->get_data();
		    $_product = wc_get_product($product_id);

		    //$tax_rates = WC_Tax::get_rates($_product->get_tax_class());
			$tax_rates = WC_Tax::get_base_tax_rates( $_product->get_tax_class( true ) );

		    $tax_rate = reset($tax_rates);
	
		    if ($tax_rate['rate'] == 22) {

		        $codice_iva = 0;


		    } elseif ($tax_rate['rate'] == 0) {

		        $codice_iva = 6;


			} elseif ($tax_rate['rate'] == 4) {

				$codice_iva = 0;

			} elseif ($tax_rate['rate'] == 10) {

			    $codice_iva = 0;

		    }

		   // $prezzo_singolo_prodotto = (round($item_data['total'], 2) / $item_data['quantity']);

			$prezzo_singolo_prodotto = ((round($item_data['total'], 2)+$item_data['total_tax']) / $item_data['quantity']);
			$ivatosiono = 'true';


		    $lista_articoli[] = array(
		        "nome" => $item_data['name'],
		        "quantita" => $item_data['quantity'],
		        "cod_iva" => $codice_iva,
		        "prezzo_netto" => $prezzo_singolo_prodotto,
		        "prezzo_lordo" => $prezzo_singolo_prodotto


		    );

		endforeach;

		

		if ($order_data['shipping_total'] > 0) {

		    $lista_articoli[] =

		        array(

		            "nome" => "Spese di Spedizione",
		            "quantita" => 1,
		            "cod_iva" => 0,
		            "prezzo_netto" => $spedizione_netta,
		            "prezzo_lordo" => $spedizione_lorda


		        );

		}

		$fattureincloud_request = array(

		    "api_uid" => $api_uid,
		    "api_key" => $api_key,
		    "nome" => $order_billing_first_name . " " . $order_billing_last_name . " " . $order_billing_company,
		    "indirizzo_via" => $order_billing_address_1,
		    "indirizzo_cap" => $order_billing_postcode,
		    "indirizzo_citta" => $order_billing_city,
		    "paese_iso" => $order_billing_country,
		    "prezzi_ivati" => $ivatosiono ,
		    "indirizzo_extra" => $order_billing_email,
		    "piva" => $order_billing_partiva,
		    "cf" => $order_billing_codfis,
		    "oggetto_visibile" => "Ordine numero ".$id_ordine_scelto,
			"mostra_info_pagamento" => true,
		    "metodo_pagamento" => "Metodo scelto". $order_billing_method,
		    "lista_articoli" => $lista_articoli,
		    "lista_pagamenti" => [
		        array(
		            "data_scadenza" => $order_data['date_created']->date('d/m/Y'),
		            "importo" => 'auto',
		            "metodo" => "Metodo scelto".$order_data['payment_method'],
		            "data_saldo" => $order_data['date_created']->date('d/m/Y'),

		        )
		    ]
		);

		$fattureincloud_options = array(
		    "http" => array(
		        "header" => "Content-type: text/json\r\n",
		        "method" => "POST",
		        "content" => json_encode($fattureincloud_request)
		    ),
		);

		$fattureincloud_context = stream_context_create($fattureincloud_options);
		$fattureincloud_result = json_decode(file_get_contents($fattureincloud_url, false, $fattureincloud_context), true);

		if( $order_complete == 'completed'){
			return $fattureincloud_result;
		}

		if (in_array("success", $fattureincloud_result)){
			$html = '<div id="message" class="notice notice-success is-dismissible">

				<p><b>Successful Creation!</b></p>
			</div>

			<script>
		        jQuery("div#message").appendTo("div#top_fattureincloud");
			</script>';
		}
		elseif (!empty($valore_paese_iso)){
			$html = '<div id="message" class="notice notice-error is-dismissible"> <p><b>Creation not Successful: Client\'s country not available</b></p></div>

			<script>
		        jQuery("div#message").appendTo("div#top_fattureincloud");
			</script>';
		}
		elseif (!empty($valore_iva)){
			$html = '<div id="message" class="notice notice-error is-dismissible">
				<p><b>Creation not Successful: Type Iva not enabled</b></p></div>

				<script>
			        jQuery("div#message").appendTo("div#top_fattureincloud");
				</script>';

		}
		elseif (!empty($valore_api_uid)){
			$html = '<div id="message" class="notice notice-error is-dismissible"><p><b>Creation not Successful: Missing or incorrect Key APIs</b></p></div>
				<script>
			        jQuery("div#message").appendTo("div#top_fattureincloud");
				</script>';
		}
		else{
			$html = '<div id="message" class="notice notice-error is-dismissible">
		<p><b>Creation not Successful:';
			$html .= $fattureincloud_result['error'];
			$html .= '<br>To verify that the settings are correct <a href="admin.php?page=woo-fattureincloud&tab=impostazioni">click here</a></p> </div>
			<script>
		        jQuery("div#message").appendTo("div#top_fattureincloud");
			</script>';
			
		}

		echo $html;
		
	}

  	/**
	 * Submit invoice to email
	 *  
	 *
	 * @since 		1.0.0
	 * @return 		void
	 */

	public static function send_inovice_email($email_array){
		
		extract($email_array);
	  	$fattureincloud_url = "https://api.fattureincloud.it:443/v1/fatture/inviamail";

		$api_uid = get_option('api_uid_mfattureincloud');
		$api_key = get_option('api_key_mfattureincloud');

		$fattureincloud_request = array(

			"id" => $idfattura,
		    "api_uid" => $api_uid,
		    "api_key" => $api_key,
			"mail_mittente"=> "no-reply@fattureincloud.it",
		    "mail_destinatario" => $email_destinatario,
			"oggetto" => $oggetto_email,
			"messaggio" => "Dear " .$nome_cliente_fic. " ,<br>\n attached the invoice dell'".$oggetto_ordine_fic." in versione PDF.
		                    <br><br>\n\nIt is also possible to download a copy by pressing the button below<br><br>\n\n{{allegati}}<br><br>\n\nBest regards
		",
			"includi_documento" => true,
			"invia_fa" => true,
			"includi_allegato" => true,
			"invia_copia" => true,
			"allega_pdf" => true

		);


		$fattureincloud_options = array(
		    "http" => array(
		        "header" => "Content-type: text/json\r\n",
		        "method" => "POST",
		        "content" => json_encode($fattureincloud_request)
		    ),
		);
		$fattureincloud_context = stream_context_create($fattureincloud_options);
		$fattureincloud_result = json_decode(file_get_contents($fattureincloud_url, false, $fattureincloud_context), true);
	  	if (in_array("success", $fattureincloud_result)):
	  		$html = '<div id="message" class="notice notice-success is-dismissible">
						<p><b>Sending Success!</b></p>
					</div>';
	  	else:
	  		$html = '<div id="message" class="notice notice-error is-dismissible">
						<p><b>Sending failed:';
			$html .= $fattureincloud_result['error'];
			$html .= '</b>
					</div>';
	  	endif;
	  	echo $html;	
	}

  public function admin_settings(){

  	
    if (isset($_POST['mw_fattureincloud_order_id'])){
                update_option('mw_fattureincloud_order_id', $_POST['mw_fattureincloud_order_id'] );
    }

    if (isset($_POST['api_uid_mfattureincloud']) && wp_verify_nonce( $_POST['_wpnonce'] )) {
        update_option('api_uid_mfattureincloud', sanitize_text_field($_POST['api_uid_mfattureincloud']));
        add_action( 'admin_notices', array($this, 'mw_admin_notices') );
    }

    if (isset($_POST['api_key_mfattureincloud']) && wp_verify_nonce( $_POST['_wpnonce'] )) {
        update_option('api_key_mfattureincloud', sanitize_text_field($_POST['api_key_mfattureincloud']));
    }

	if (isset($_POST['mw_fattureincloud_anno_fatture']) && wp_verify_nonce( $_POST['_wpnonce'] )) {
		update_option('mw_fattureincloud_anno_fatture', sanitize_text_field($_POST['mw_fattureincloud_anno_fatture']));
	}

	if (isset($_POST['mw_fattureincloud_anno_fatture']) && wp_verify_nonce( $_POST['_wpnonce'] )) {
		update_option('mw_fattureincloud_anno_fatture', sanitize_text_field($_POST['mw_fattureincloud_anno_fatture']));
	}

    if (isset($_POST['mw_fattureincloud_order_save'])) {
        update_option('mw_fattureincloud_order_save', $_POST['mw_fattureincloud_order_save']);
        add_action( 'admin_notices', array($this, 'mw_admin_notices') );
        
    }

    if (isset($_POST['mw_fattureincloud_auto_save'])) {
        update_option('mw_fattureincloud_auto_save', $_POST['mw_fattureincloud_auto_save']);
        add_action( 'admin_notices', array($this, 'mw_admin_notices') );
        
    }

    if (isset($_POST['mw_fattureincloud_partiva_codfisc'])) {
        update_option('mw_fattureincloud_partiva_codfisc', $_POST['mw_fattureincloud_partiva_codfisc']);
	    add_action( 'admin_notices', array($this, 'mw_admin_notices') );
    }

    if(isset($_POST['delete_autosave_fattureincloud'])) {
        delete_option('fattureincloud_autosent_id_fallito');
        add_action( 'admin_notices', array($this, 'mw_admin_notices') );
    }

  }

  public function mw_admin_notices(){
  		$type = 'updated';
        $message = __( 'Updated Value', 'fattureincloud' );
        add_settings_error('fattureincloud',esc_attr( 'settings_updated' ),$message, $type);
        settings_errors('fattureincloud');
    }

}