<div id="woo_fattureincloud">
    <header></header>
</div>

<h2> Enter the site API fattureincloud.it: API >> SHOW API UID AND KEY API </h2>
<hr>
<?php

	/**
	 *
	 * Form for API Value API UID and API KEY
	 *
	 */
	?>
    <form id="woo-fattureincloud-settings_key" method="POST">
        <table border="0" cellpadding="6">
            <tr>
                <td align="right">
                    <?php wp_nonce_field(); ?>
                    <label for="woo_fattureincloud_apiuid">API UID</label>

                    <input type="password" name="api_uid_mfattureincloud" placeholder="api uid"
                    value="<?php echo get_option('api_uid_mfattureincloud'); ?>">

                    <label for="woo_fattureincloud_apikey">API KEY</label>

                    <input type="password" name="api_key_mfattureincloud" placeholder="api key"
                    value="<?php echo get_option('api_key_mfattureincloud'); ?>">

                </td>
                <td>
                    <input type="submit" value="Save" class="button button-primary button-large">
                </td>
            </tr>
<!--            <tr>
                <td align="right">
                    <label  for="mw_fattureincloud_order_save"><?php echo __( 'Enables automatic creation of the invoice on invoiceincloud.it when the order is submitted', 'woo-fattureincloud' );?></label>
                    <input type="hidden" name="mw_fattureincloud_order_save" value="0" />
                    <input type="checkbox" name="mw_fattureincloud_order_save" id="mw_fattureincloud_order_save" value="1" <?php if ( 1 == get_option('mw_fattureincloud_order_save') ){ echo 'checked'; } else{ echo '';} ?>>

                </td>
                <td>
                    <input type="submit" value="Save" class="button button-primary button-large">
                </td>
            
            </tr> 
-->
            <tr>
                <td align="right">
                    <label  for="mw_fattureincloud_auto_save"><?php echo __( 'Enables automatic creation of the invoice on invoiceincloud.it when the order is completed', 'woo-fattureincloud' );?></label>
                    <input type="hidden" name="mw_fattureincloud_auto_save" value="0" />
                    <input type="checkbox" name="mw_fattureincloud_auto_save" id="mw_fattureincloud_auto_save" value="1" <?php if ( 1 == get_option('mw_fattureincloud_auto_save') ){ echo 'checked'; } else{ echo '';} ?>>

                </td>
                <td>
                    <input type="submit" value="Save" class="button button-primary button-large">
                </td>
            
            </tr> 
            <tr>
                <td align="right">
                   <label for="mw_fattureincloud_partiva_codfisc"><?php echo __( 'Activate the item VAT and Tax Code in the Checkout of WooFatture', 'woo-fattureincloud' );?></label>
                   <input type="hidden" name="mw_fattureincloud_partiva_codfisc" value="0" />
                   <input type="checkbox" name="mw_fattureincloud_partiva_codfisc" id="mw_fattureincloud_partiva_codfisc" value="1" <?php if ( 1 == get_option('mw_fattureincloud_partiva_codfisc') ){ echo 'checked'; } else { echo ''; } ?>>
                </td>
                <td>
                    <input type="submit" value="Save" class="button button-primary button-large">
                </td>
            
            </tr>                
            <tr>
                <td>
                    <b>Alternatively, you can use the WooCommerce POVIVA plugin and Fiscal Code for Italy</i>
                </td>
            </tr>
        </table>
    </form>