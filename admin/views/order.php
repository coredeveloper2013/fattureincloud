
	<?php wp_nonce_field(); ?>
	<table border="0" width=80% cellpadding="14" cellspacing="4" class="product-display">
		<tr>
			<?php

				$latest_order_id = get_last_order_id(); // Last order ID

				if ($latest_order_id == '') {
					echo "<p>no order present</p>";
			?>
					<div id="message" class="notice notice-error is-dismissible">
						<p><b>There are no WooCommerce Orders!</b></p>
					</div>
					<script>
						jQuery("div#message").appendTo("div#top_fattureincloud");
					</script>
			<?php

					exit;
					
				}
				$args = array(
					'post_type' => 'shop_order',
					'posts_per_page' => 10,
					'post_status' => array_keys(wc_get_order_statuses())
				);
			?>
			<td align="center">
				<form id="woo-fattureincloud-preview" action="" method="POST">
					<select name="mw_fattureincloud_order_id">
						<?php
							$orders = get_posts($args);
							if (get_option('mw_fattureincloud_order_id') == null):
						?>
								<option value="<?php echo $latest_order_id; ?>" selected="selected">Selected: <?php echo $latest_order_id; ?></option>
						<?php else: ?>
								<option value="" selected="selected">
								Selected <?php echo get_option('mw_fattureincloud_order_id'); ?></option>
						<?php endif; ?>

						<?php foreach ($orders as $order): ?>
								<option value="<?php echo $order->ID; ?>">ID ordine : <?php echo $order->ID; ?></option>

						<?php endforeach; ?>
					</select>
					<button type="submit" name="submit" value="" class="button button-primary">Select</button>
				</form>
			</td>
		</tr>
		<tr>
			<td colspan="3" bgcolor="FFFFFF" align="right">
				<?php
					if (get_option('mw_fattureincloud_order_id') == null){

						$id_ordine_scelto = $latest_order_id;
					}
					else {

						$id_ordine_scelto = get_option('mw_fattureincloud_order_id') ;

					}

					if (get_post_meta( $id_ordine_scelto, '_billing_piva', true ) || get_post_meta( $id_ordine_scelto, '_billing_cf', true ) ) {

						$order_billing_partiva = get_post_meta( $id_ordine_scelto, '_billing_piva', true ) ;
						$order_billing_codfis = get_post_meta( $id_ordine_scelto, '_billing_cf', true ) ;
					}
					elseif (get_post_meta( $id_ordine_scelto, '_billing_partita_iva', true ) || $order_billing_codfis = get_post_meta( $id_ordine_scelto, '_billing_cod_fisc', true )) {
						$order_billing_partiva = get_post_meta( $id_ordine_scelto, '_billing_partita_iva', true ) ;
						$order_billing_codfis = get_post_meta( $id_ordine_scelto, '_billing_cod_fisc', true ) ;
					}
					else {
						$order_billing_partiva ="";
						$order_billing_codfis = "";
					}

					$order = wc_get_order( $id_ordine_scelto);

					$order_data = $order->get_data(); // The Order data
					$order_shipping_total = $order_data['shipping_total'];
					$order_shipping_tax = $order_data['shipping_tax'];
					$order_total = $order_data['total'];
					$order_total_tax = $order_data['total_tax'];
					$fattureincloud_iva = 22;
					$ivaDivisore = 1 + ($fattureincloud_iva / 100);
					$order_total_partial = $order_total / $ivaDivisore;
					$order_total_partial = round($order_total_partial, 2);
					$totale_iva_fattureincloud = $order_total - $order_total_partial;
					$totale_esclusaiva = $order_total  - $order_total_tax;

					## BILLING INFORMATION:

					$order_billing_first_name = $order_data['billing']['first_name'];
					$order_billing_last_name = $order_data['billing']['last_name'];
					$order_billing_company = $order_data['billing']['company'];
					$order_billing_address_1 = $order_data['billing']['address_1'];
					$order_billing_address_2 = $order_data['billing']['address_2'];
					$order_billing_city = $order_data['billing']['city'];
					$order_billing_state = $order_data['billing']['state'];
					$order_billing_postcode = $order_data['billing']['postcode'];
					$order_billing_country = $order_data['billing']['country'];
					$order_billing_email = $order_data['billing']['email'];
					$order_billing_phone = $order_data['billing']['phone'];
					$order_billing_method = $order_data['payment_method_title'];

					echo "<b>Recipient</b> 
					<br><b>".__('Name','woo-fattureincloud')."</b> ".$order_billing_first_name." ".$order_billing_last_name.
					"<br><b>".__('Company','woo-fattureincloud')."</b> ".$order_billing_company.
					"<br><b>".__('Address','woo-fattureincloud')."</b> ".$order_billing_address_1.
					"<br><b>".__('City','woo-fattureincloud')."</b> ".$order_billing_city.
					"<br><b>".__('Postal code','woo-fattureincloud')."</b> ".$order_billing_postcode.
					"<br><b>".__('Country','woo-fattureincloud')."</b> ".$order_billing_country.
					"<br><b>".__('VAT number','woo-fattureincloud')."</b> ".$order_billing_partiva.
					"<br><b>".__('Tax code','woo-fattureincloud')."</b> ".$order_billing_codfis.
					     // "<pre>".print_r($order_data)."</pre>".
					"<br><b>".__('Payment Method','woo-fattureincloud')."</b> ".$order_billing_method."

					</td></tr>

					<tr>
					<td colspan=\"3\" bgcolor=\"FFFFFF\">

					<b>Product List</b><hr>";
					// Iterating through each WC_Order_Item_Product objects
					foreach ($order->get_items() as $item_key => $item_values){

						## Using WC_Order_Item methods ##

						// Item ID is directly accessible from the $item_key in the foreach loop or
						$item_id = $item_values->get_id();



						## Using WC_Order_Item_Product methods ##

						$item_name = $item_values->get_name(); // Name of the product
						$item_type = $item_values->get_type(); // Type of the order item ("line_item")

						$product_id = $item_values->get_product_id(); // the Product id
						$wc_product = $item_values->get_product(); // the WC_Product object
						## Access Order Items data properties (in an array of values) ##
						$item_data = $item_values->get_data();
						$_product = wc_get_product( $product_id );

						$product_name = $item_data['name'];
						$product_id = $item_data['product_id'];
						$variation_id = $item_data['variation_id'];
						$quantity = $item_data['quantity'];
						$tax_class = $item_data['tax_class'];
						$line_subtotal = $item_data['subtotal'];
						$line_subtotal_tax = $item_data['subtotal_tax'];
						$line_total = $item_data['total'];
						$line_total_tax = $item_data['total_tax'];
						$prezzo_singolo_prodotto = $line_total/$quantity;
						$prezzo_singolo_prodotto = $prezzo_singolo_prodotto/ $ivaDivisore;
						$prezzo_singolo_prodotto = round($prezzo_singolo_prodotto, 2);
						$item_tax_class = $item_data['tax_class'];

						//$tax_rate = array();
						//$tax_rates = WC_Tax::get_rates( $_product->get_tax_class() );
						$tax_rates = WC_Tax::get_base_tax_rates( $_product->get_tax_class( true ) );


						if (!empty($tax_rates)) {
							$tax_rate = reset($tax_rates);


							echo "Tax rate ". round($tax_rate['rate'], 0)."%<br>";
						}
						$_product = reset($_product );

						echo"<b>".__('Product name','woo-fattureincloud')."</b> ". $product_name."<br>";

						$product = wc_get_product( $product_id );
						echo "<b>".__('Product Image','woo-fattureincloud')."</b> ".$product->get_image().'<br>';
						/* echo $order_data['date_created']->date('d/m/Y')."<br>";*/
						echo "<b>".__('Price','woo-fattureincloud')."</b> ".$product->get_price_html()."<br>


						<b>".__('Quantity','woo-fattureincloud')."</b> ".$quantity."<br>".

						"<b>".__('Sub Total','woo-fattureincloud')."</b> €".round($line_total, 2).
						"<hr>";
					}




				echo    "</td>
				</tr>
				<tr>
				<td colspan='3' align='right' bgcolor='FFFFFF'>


				<br><br><b>".__('Order Number','woo-fattureincloud')."</b> ". $id_ordine_scelto.
				"<br><b>".__('Cost Shipping','woo-fattureincloud')."</b> ". $order_shipping_total.
				"<br><b>".__('VAT Shipping 22%','woo-fattureincloud')."</b> ". $order_shipping_tax.
				"<br><b>".__('Total VAT excluded','woo-fattureincloud')."</b> ".  $totale_esclusaiva.
				"<br><b>".__('Taxes','woo-fattureincloud')."</b> ". $order_total_tax.
				"<br><b>".__('Total','woo-fattureincloud')."</b> ". $order_total;
				?>
			</td>
		</tr>
		<tr>
			<td colspan='3' align='right'>
				<?php
					if ( get_option('woocommerce_prices_include_tax') == 'no'){echo "<form method=\"POST\">
						<button type=\"submit\" name=\"submit_send_fattureincloud\" value=\"Seleziona\" class=\"button button-primary\">Create the Invoice on Fattureincloud</button>
						</form>";
					}
					elseif ( get_option('woocommerce_prices_include_tax') == 'yes'){
				?>
					<button type="submit" name="submit_send_fattureincloud" value="Seleziona" class="button button-primary" disabled>Create the Invoice on Fattureincloud</button>

					<div id="message" class="notice notice-error">
						<p><b>Per utilizzare questo plugin è necessario impostare i <a href="admin.php?page=wc-settings&tab=tax">prezzi al netto dell'imposta</a> | <a href="https://woofatture.com/documentazione/#nettoiva">Maggiori informazioni</a></b> </p>
					</div>

					<div id="message2" class="notice notice-error">
						<p><b>Per utilizzare questo plugin è necessario impostare i <a href="admin.php?page=wc-settings&tab=tax">prezzi al netto dell'imposta</a> | <a href="https://woofatture.com/documentazione/#nettoiva">Maggiori informazioni</a></b></p>
					</div>

					<script>
						jQuery("div#message").appendTo("div#top_fattureincloud");
					</script>

				<?php

					}

					if (isset($_POST['submit_send_fattureincloud'])) {
						Fattureincloud_Admin::process_order_invoice_to_fatture($order,$order_data);

					}
				?>
			</td>
		</tr>
	</table>
</form>