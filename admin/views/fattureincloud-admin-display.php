<?php

// Don't access this directly, please

if ( ! defined( 'ABSPATH' ) ) exit;

?>



<?php
/*
 *
 * Controllo API KEY e mostra messaggio se mancano
 *
 *
 */
if (get_option('api_uid_mfattureincloud') == null || get_option('api_key_mfattureincloud') == null ) {
	?>
    <div id="message" class="notice notice-error is-dismissible"> <p><a href="<?php echo admin_url('admin.php?page=fattureincloud&tab=settings');?>">Click Here</a> to Verify that the KEY APIs have been inserted</p>
    </div>
<?php }

/*
 *
 * Controllo mancato invio automatico Fattura al cambio di stato Completato e mostra messaggio di errore
 *
 *
 */

 if (get_option('fattureincloud_autosent_id_fallito')!='')

{
	?>
	<div id="message" class="notice notice-error">
		<p><b>Invio automatico ordine n <?php echo get_option('fattureincloud_autosent_id_fallito'); ?>  non Riuscito  <a href="https://woofatture.com/documentazione/">HELP</a></b>


		<form method="POST">
			<input type="hidden" name="delete_autosave_fattureincloud" />
			<input type="submit" value="Cancella" class="button button-small ">
		</form>

		</p>
	</div>
	<?php

}



// Code displayed before the tabs (outside)
// Tabs
?>
<div id="top_fattureincloud"></div>
<h1><?php echo __( 'WooCommerce Fattureincloud', 'woo-fattureincloud' );?></h1>

<?php

		$tab = ( ! empty( $_GET['tab'] ) ) ? esc_attr( $_GET['tab'] ) : 'order';

		Fattureincloud_Admin::page_tabs( $tab );

		if ( $tab == 'order' ) {
			include_once  plugin_dir_path(__FILE__) . 'order.php';
		}
		elseif ( $tab == 'invoice' ){
			include_once plugin_dir_path(__FILE__) . 'fatture.php';
		}
		elseif ( $tab == 'email' ){
			include_once plugin_dir_path(__FILE__) . 'get_email_fattureincloud.php';
		}
		else {
			include_once plugin_dir_path(__FILE__) . 'settings.php';
		}


