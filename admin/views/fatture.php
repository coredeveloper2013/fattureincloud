<?php if (!defined('ABSPATH')) exit; ?>

<table border="0" cellpadding="6">
    <tr>
        <td align="right">
        	<form id="woo-fattureincloud-anno-fatture" method="POST">
		    	<?php wp_nonce_field(); ?>
	            <label for="woo_fattureincloud_apiuid">Year Bills</label>
	            <input type="number" name="mw_fattureincloud_anno_fatture" placeholder="anno" value="<?php echo get_option('mw_fattureincloud_anno_fatture'); ?>">
        	<input type="submit" value="Save" class="button button-primary button-large">
        	</form>
        </td>
	</tr>
</table>

<div id="fatture-elenco">
	<?php

		$api_uid = get_option('api_uid_mfattureincloud');
		$api_key = get_option('api_key_mfattureincloud');
		$annofatture = get_option('mw_fattureincloud_anno_fatture');

		$url = "https://api.fattureincloud.it:443/v1/fatture/lista";
		$request = array(
			"api_uid" => $api_uid,
			"api_key" => $api_key,
			"anno" => $annofatture

		);
		$options = array(
			"http" => array(
				"header"  => "Content-type: text/json\r\n",
				"method"  => "POST",
				"content" => json_encode($request)
			),
		);

		$context  = stream_context_create($options);
		$result = json_decode(file_get_contents($url, false, $context), true);

		if (is_array($result)){
			foreach ($result as $value){

				if (is_array($value)) {
					$count = 0;
					foreach ($value as $value2){

						$count = $count + 1;

						$idfattura = $value2['id'];

						$url_dett = "https://api.fattureincloud.it:443/v1/fatture/dettagli";
						$request_dett = array(
							"api_uid" => $api_uid,
							"api_key" => $api_key,
							"id" => $idfattura

						);
						$options_dett = array(
							"http" => array(
								"header"  => "Content-type: text/json\r\n",
								"method"  => "POST",
								"content" => json_encode($request_dett)
							),
						);
						$context_dett  = stream_context_create($options_dett);
						$result_dett = json_decode(file_get_contents($url_dett, false, $context_dett), true);
						$email_array = array();
						if (is_array($result_dett))
						{
							foreach ($result_dett as $value_dett) {

								if (!empty($value_dett['indirizzo_extra'])) {


									echo "<form id=\"send_email_fattureincloud$idfattura\" method=\"POST\">";



									print "<a href=\"https://secure.fattureincloud.it/invoices-view-".$value_dett['id']."\">View invoice</a><br>";
									print "<b>".$value_dett['oggetto_visibile']."</b>";
									print " Recipient: ".$value_dett['nome']."";
									print " | <b>VAT amount included</b> €".$value_dett['importo_totale']." <br> ";
									print "<b>email</b> ".$value_dett['indirizzo_extra']."<br>";

									

									$email_array['idfattura'] = $idfattura;
									
									$email_array['oggetto_email'] = $value_dett['oggetto_visibile'];
									$email_destinatario = $value_dett['indirizzo_extra'];
									$email_array['email_destinatario'] = $value_dett['indirizzo_extra'];

									$email_array['nome_cliente_fic'] = $value_dett['nome'];

									$email_array['oggetto_ordine_fic'] = $value_dett['oggetto_visibile'];

									echo "<input type=\"hidden\" value=\"$email_destinatario\" name=\"email_destinatario\" />";

									echo "<button type=\"submit\" name=\"$idfattura\" value=\"$idfattura\" class=\"button button-primary\" >Send Invoice via Email</button><hr>";

									if (isset($_POST[$idfattura])) {
										Fattureincloud_Admin::send_inovice_email($email_array);

									}

									echo "</form><br>";
								}



							}

						}


						if ($count == 20) {

							print "maximum number ( 20 ) of viewable invoices reached";
							break;

						}

						else {

						}
					}

				}
			}
		}
	?>
</div>
<?php
	if (in_array("success", $result)) {
	}
	else{
?>
	<div id="message" class="notice notice-error is-dismissible">
		<p> <b> List of Invoices Not Downloaded: <?php echo $result['error']; ?> </b> </p>
	</div>
<?php
}