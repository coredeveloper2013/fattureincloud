<?php
// If uninstall not called from WordPress exit
if( !defined( 'WP_UNINSTALL_PLUGIN' ) )
	exit ();

// Delete option from options table
delete_option( 'api_key_mfattureincloud' );

delete_option( 'api_uid_mfattureincloud' );

delete_option( 'mw_fattureincloud_order_id' );

delete_option( 'mw_fattureincloud_auto_save' );

delete_option( 'mw_fattureincloud_anno_fatture' );

delete_option( 'mw_fattureincloud_partiva_codfisc' );


//remove any additional options and custom tables
