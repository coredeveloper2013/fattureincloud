<?php
/**
 * WordPress plugin "Fattureincloud" main file, responsible for initiating the plugin.
 *
 * Developer and company Info
 * @company Mediusware
 * @link    https://simonechinaglia.me
 * @package Fattureincloud
 * @author Simone Chinaglia
 * @version 1.0.0
 */

/*
 * Plugin Name: fattureincloud
 * Description: WooCommerce Fattureincloud integration
 * Version:     1.0.0
 * Contributors: simone.chinaglia
 * Author:      Simone Chinaglia
 * Author URI:        https://simonechinaglia.me
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: fattureincloud
 * Domain Path: /languages
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0
 * Rename this for your plugin and update it when you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-fattureincloud-activator.php
 */
function activate_fattureincloud() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-fattureincloud-activator.php';
	Fattureincloud_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-fattureincloud-deactivator.php
 */
function deactivate_fattureincloud() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-fattureincloud-deactivator.php';
	Fattureincloud_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_fattureincloud' );
register_deactivation_hook( __FILE__, 'deactivate_fattureincloud' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-fattureincloud.php';
require plugin_dir_path( __FILE__ ) . 'includes/helpers.php';
//print_r(get_last_order_id());
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_fattureincloud() {

	$plugin = new Fattureincloud();
	$plugin->run();

}

// run the plugin  run fattureincloud initiate.
run_fattureincloud();